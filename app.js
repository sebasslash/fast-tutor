//Application Package Requirements
var express        = require('express'),
    mongoose       = require('mongoose'),
    passport       = require('passport'),
    LocalStrategy  = require('passport-local'),
    bodyParser     = require('body-parser'),
    methodOverride = require('method-override'),
    expressSanitizer = require('express-sanitizer'),
    Tutor            = require('./models/tutor'),
    Student          = require('./models/student'),
    app            = express();
//Route Requirements
var studentRoutes = require('./routes/students'),
    tutorRoutes   = require('./routes/tutors'),
    authRoutes    = require('./routes/auth'),
    indexRoutes   = require('./routes/index');


mongoose.connect("mongodb://master://#Jayuya09@ds119728.mlab.com:19728/tutormenow");

//Application SETUP
app.set("view engine", "ejs");  //EJS Templates Setup
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname+ "/public")); //Public Directory ==> CSS, FrontendJS
app.use(expressSanitizer()); //Sanitize the App from script injections
//app.use(methodOverride("_method"));

app.use(require('express-session')({
    secret: "I love Jessie with all my life",
    resave: false,
    saveUninitialized: false
}));

//Auth Setup
app.use(passport.initialize());
app.use(passport.session());


//////////////////////////////////////////////////////////////////////
//Student Auth Setup
//////////////////////////////////////////////////////////////////////
passport.use(new LocalStrategy(Student.authenticate()));
passport.serializeUser(Student.serializeUser());
passport.deserializeUser(Student.deserializeUser());

app.use(function(req,res,next){
    res.locals.currentStudent = req.student;
    next();
});
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//Teacher Auth Setup/////////////
//////////////////////////////////////////////////////////////////////
passport.use(new LocalStrategy(Tutor.authenticate()));
passport.serializeUser(Tutor.serializeUser());
passport.deserializeUser(Tutor.serializeUser());

app.use(function(req,res,next){
    res.locals.currentTutor = req.tutor;
    next();
});
//////////////////////////////////////////////////////////////////////


//app.use(indexRoutes);
app.use(studentRoutes);
app.use(authRoutes);
app.use(tutorRoutes);
app.use(indexRoutes);

//app.listen(process.env.PORT, process.env.IP);
app.listen(3000, function(){
    console.log("Server is running");
});
