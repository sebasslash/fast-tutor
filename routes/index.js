var express = require('express'),
    router = express.Router({mergeParams:true}),
    Student = require('../models/student');

//Home Page
router.get("/", function(req,res){
    res.render("index/home");
});
router.get("/register", function(req,res){
    res.render("index/register");
});
router.get("*", function(req,res){
    res.render("index/error");
});


module.exports = router;
