var express = require('express');
    router  = express.Router({mergeParams:true});

router.get('/student', function(req,res){
    res.render("students/registerStudent");
});
router.get('/student/test', function(req,res){
    res.render("students/test");
});

module.exports = router;
