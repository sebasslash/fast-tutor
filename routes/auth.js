var express = require('express'),
    router  = express.Router();
// var bodyParser = require('body-parser');
var Student = require('../models/student');
var Tutor   = require('../models/tutor');
var passport = require('passport');
var error = false;



//outer.use(bodyParser.urlencoded({extended:true}));

///////////////////////////////////////////////////////////////////////
//Handles REGISTRATIONS
/////////////////////////////////////////////////////////////////////
router.get("/register/student", function(req,res){
    res.render("students/registerStudent");
});
if(error){
    router.get("/register/student/error", function(req,res){
        res.render("students/registerStudentRetry");
    });
}
router.post("/register/student", function(req,res){
    console.log("register");
    studentData = req.body.student;
    if(Student.find({}) == 0 && (req.body.username == Student.findOne({username:req.body.username})
    || req.body.student.email == Student.findOne({email:req.body.student.email}))){
        error = true;
        res.redirect("/register/student/error");
    }else{
        error = false;
        Student.register(new Student({username: req.body.userName}),req.body.password, function(err, student){
                if(err){
                    console.log(err);
                }else{
                     //Authenticate the User
                     console.log(student);
                     //Add missing User Content
                     Student.findByIdAndUpdate(student._id,studentData, function(err, student){
                            if(err){
                                console.log(err);
                            } else{
                                student.save();
                                passport.authenticate("local")(req,res,function(){
                                    res.redirect("/");
                                });
                            }
                      });
                 }
             });
    }


});
router.get("/register/tutor", function(req,res){
    res.render("tutors/registerTutor");
});
router.post("/register/tutor", function(req,res){
    tutorData = req.body.tutor;
    Tutor.register(new Tutor({username: req.body.userName}), req.body.password, function(err,tutor){
        if(err){
            res.redirect("/register/tutor");
        }else{
            //Add missing Tutor Content to the account with password and username
            Tutor.findByIdAndUpdate(tutor._id, tutorData,function(err,tutor){
                if(err){
                    console.log(err);
                }else{
                    tutor.save();
                    passport.authenticate("local")(req,res,function(){
                        res.redirect("/");
                    });
                }
            });
        }
    });
});

module.exports = router;
