var mongoose = require('mongoose');
var passportLocal = require('passport-local-mongoose');
var tutorSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    userName: String,
    password: String,
    email: String,
    rating: Number
});
tutorSchema.plugin(passportLocal);
module.exports = mongoose.model("Tutor", tutorSchema);
