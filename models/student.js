///////////////////////////////////////////////////////

var mongoose = require('mongoose');
var passportLocal = require('passport-local-mongoose');

///////////////////////////////////////////////////////

//Create the Student Schema
var studentSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    username: String,
    password: String,
    email: String
    // subjects:[
    //     math: Boolean,
    //     science:
    // ]
});

studentSchema.plugin(passportLocal);

module.exports = mongoose.model("Student", studentSchema)
